package org.osmtools.josmpresets;

import java.io.File;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import org.junit.Assert;
import junit.framework.TestCase;

public class IconExistTest extends TestCase {

    public IconExistTest(String name) {
        super(name);
    }
	
    public void testParser() throws Exception {
		try {
			SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();

			saxParser.parse(new File("preset.xml"), new DefaultHandler() {
				public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
					if ("group".equals(qName) || "item".equals(qName)) {
						String url = attributes.getValue("icon");
						Assert.assertNotNull(url);
						Assert.assertFalse(url.isEmpty());
						File f = new File(url);
						Assert.assertTrue(url+" not found", f.exists());
					}
					
					// labels have an optional icon. if it's specified it must exist
					if ("label".equals(qName)) {
						String url = attributes.getValue("icon");
						if (url != null) {
							Assert.assertFalse(url.isEmpty());
							File f = new File(url);
							Assert.assertTrue(url+" not found", f.exists());
						}
					}
				}
			});
		} catch (Exception e) {
			Assert.fail("Exception during file parsing: "+e.getMessage());
		}
    }
}
