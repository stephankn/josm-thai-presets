JOSM presets for Thailand
=========================

## About ##

This archive contains presets for the OpenStreetMap Editor [JOSM](http://josm.openstreetmap.org) which are considered useful for mapping in Thailand.

The selection of presets is based on my experience of what is useful and on
feedback from other mappers.
The tagging style used is based on established standards. But keep in mind
that it is nothing authoritative.

## Download & Installation ##
The presets are automatically build and tested after every commit using Bitbucket Pipelines.
After a successful build/test an updated version is automatically available at [https://code.osm-tools.org/latest/josm-thai-presets.zip](https://code.osm-tools.org/latest/josm-thai-presets.zip).

### JOSM ###
To install it in JOSM add this URL to the presets in `Preferences/Map Settings/Tagging Presets` using the "*plus*" button on the right hand side.

### Vespucci ###
Open `Preferences/Advanced preferences/Presets/Add preset` and add this URL and the name "*Thai Presets*".

## Development ##
A git repository of this preset is available at [https://bitbucket.org/stephankn/josm-thai-presets](https://bitbucket.org/stephankn/josm-thai-presets).

You can use the issue tracker on that site to send me bug reports or requests for enhancements. 

Maven is supported for XML validation and packaging:

    mvn xml:validate
	mvn test
    mvn assembly:single


## Icon sources ##

These are the sources of the icons used. Logos and trademarks are only used for identifying the brand in the tagging presets in a low-resolution variant on a fair-use basis. Logos of the features belong to the companies.

If you don't want your file/logo used here, please let me know: [josm-presets@osm-tools.org](mailto:josm-presets@osm-tools.org)

Additional images are:

**office-building icon**
> CC-BY-SA 3.0 [http://mapicons.nicolasmollet.com/](http://mapicons.nicolasmollet.com/)


**fuel vending machine**
> CC-BY-SA 4.0 [https://commons.wikimedia.org/wiki/File:Vending_machine_for_motor_fuel,_Thailand.jpg](https://commons.wikimedia.org/wiki/File:Vending_machine_for_motor_fuel,_Thailand.jpg)


**Flag of Thailand**
> Public Domain, [http://commons.wikimedia.org/wiki/File:Flag_of_Thailand.svg](http://commons.wikimedia.org/wiki/File:Flag_of_Thailand.svg)

**convenience, fuel, dollar, bank, atm, car, motorbike, restaurant**
> Preset icons from JOSM repository for consistent representation of presets. Images are GPL v2 or newer


### Image compression ###
To have a small file size all logos are reduced to 64px by 64px and then saved as a paletted png with optimized compression. File name is value of main key and name of feature. For example "**fuel_PTT.png**".

    pngquant.exe --speed=1 --quality=65-80 -v shop_7eleven.png
    TruePNG.exe /o max shop_bigc-fs8.png
    PNGZopfli.exe shop_7eleven-fs8.png 1000